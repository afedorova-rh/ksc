%global forgeurl https://github.com/RedHatOfficial/ksc
%global commitdate 20210517
%global commit b99026dfbdc14bda13061a318466ed4bd83e0de1
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%{?python_enable_dependency_generator}
%forgemeta -i

Name:		ksc
Version:	1.8
Release:	2%{?dist}
Summary:	Kernel source code checker
Group:		Development/Tools
AutoReqProv:	no
License:	GPLv2+
URL:		https://github.com/RedHatOfficial/ksc
BuildArch:	noarch
Requires:	kmod
Requires:	binutils
Requires:	kernel-devel
Requires:	python3-requests
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Source0:	https://github.com/RedHatOfficial/ksc/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

%description
A kernel module source code checker to find usage of select symbols

%prep
%forgesetup

%build
%py3_build

%install
%py3_install
install -D ksc.1 %{buildroot}%{_mandir}/man1/ksc.1

%files
%license COPYING
%doc README PKG-INFO
%{_bindir}/ksc
%{_datadir}/ksc
%{_mandir}/man1/ksc.*
%config(noreplace) %{_sysconfdir}/ksc.conf
%{python3_sitelib}/ksc-%{version}*.egg-info

%changelog
* Tue Jun 01 2021 Ziqian SUN <zsun@redhat.com> - 1.8-2
- Adding python3-requests into Requires.

* Mon May 17 2021 Čestmír Kalina <ckalina@redhat.com> - 1.8-1
- Resolves: #1954495 ksc: i18n issues

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - Forge-specific packaging variables
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Tue Jan 05 2021 Čestmír Kalina <ckalina@redhat.com> - 1.7-1
- Initial Fedora commit.
